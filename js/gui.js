const UserMove = {};
UserMove.from = SQUARES.NO_SQ;
UserMove.to = SQUARES.NO_SQ;

const MirrorFiles = [FILES.FILE_H, FILES.FILE_G, FILES.FILE_F, FILES.FILE_E, FILES.FILE_D, FILES.FILE_C, FILES.FILE_B, FILES.FILE_A];
const MirrorRanks = [RANKS.RANK_8, RANKS.RANK_7, RANKS.RANK_6, RANKS.RANK_5, RANKS.RANK_4, RANKS.RANK_3, RANKS.RANK_2, RANKS.RANK_1];

function MIRROR120(sq) {
  const file = MirrorFiles[FilesBrd[sq]];
  const rank = MirrorRanks[RanksBrd[sq]];
  return FR2SQ(file, rank);
}

$("#SetFen").click(function () {
  const fenStr = $("#fenIn").val();
  ParseFen(fenStr);
  PrintBoard();
  SetInitialBoardPieces();
  GameController.PlayerSide = brd_side;
  CheckAndSet();
  EvalPosition();
  newGameAjax();
});

/**
 * @return {number}
 */
function CheckResult() {

  if (brd_fiftyMove > 100) {
    $("#GameStatus").text("GAME DRAWN {fifty move rule}");
    return BOOL.TRUE;
  }

  if (ThreeFoldRep() >= 2) {
    $("#GameStatus").text("GAME DRAWN {3-fold repetition}");
    return BOOL.TRUE;
  }

  if (DrawMaterial() === BOOL.TRUE) {
    $("#GameStatus").text("GAME DRAWN {insufficient material to mate}");
    return BOOL.TRUE;
  }

  console.log('Checking end of game');
  GenerateMoves();

  let MoveNum = 0;
  let found = 0;
  for (MoveNum = brd_moveListStart[brd_ply]; MoveNum < brd_moveListStart[brd_ply + 1]; ++MoveNum) {

    if (MakeMove(brd_moveList[MoveNum]) === BOOL.FALSE) {
      continue;
    }
    found++;
    TakeMove();
    break;
  }

  $("#currentFenSpan").text(BoardToFen());

  if (found !== 0) return BOOL.FALSE;
  const InCheck = SqAttacked(brd_pList[PCEINDEX(Kings[brd_side], 0)], brd_side ^ 1);
  console.log('No Move Found, incheck:' + InCheck);

  if (InCheck === BOOL.TRUE) {
    if (brd_side === COLOURS.WHITE) {
      $("#GameStatus").text("GAME OVER {black mates}");
      return BOOL.TRUE;
    } else {
      $("#GameStatus").text("GAME OVER {white mates}");
      return BOOL.TRUE;
    }
  } else {
    $("#GameStatus").text("GAME DRAWN {stalemate}");
    return BOOL.TRUE;
  }
  console.log('Returning False');
  return BOOL.FALSE;
}

function ClickedSquare(pageX, pageY) {
  const position = $("#Board").position();
  console.log("Piece clicked at " + pageX + "," + pageY + " board top:" + position.top + " board left:" + position.left);

  const workedX = Math.floor(position.left);
  const workedY = Math.floor(position.top);
  pageX = Math.floor(pageX);
  pageY = Math.floor(pageY);

  const file = Math.floor((pageX - workedX) / 60);
  const rank = 7 - Math.floor((pageY - workedY) / 60);

  let sq = FR2SQ(file, rank);


  if (GameController.BoardFlipped === BOOL.TRUE) {
    sq = MIRROR120(sq);
  }

  console.log("WorkedX: " + workedX + " WorkedY:" + workedY + " File:" + file + " Rank:" + rank);
  console.log("clicked:" + PrSq(sq));

  SetSqSelected(sq); // must go here before mirror

  return sq;

}

function CheckAndSet() {
  if (CheckResult() !== BOOL.TRUE) {
    GameController.GameOver = BOOL.FALSE;
    $("#GameStatus").text('');
  } else {
    GameController.GameOver = BOOL.TRUE;
    GameController.GameSaved = BOOL.TRUE; // save the game here
  }
  //var fenStr = BoardToFen();
  $("#currentFenSpan").text(BoardToFen());
}

function PreSearch() {

  if (GameController.GameOver !== BOOL.TRUE) {
    srch_thinking = BOOL.TRUE;
    $('#ThinkingImageDiv').append('<image src="images/think3.png" id="ThinkingPng"/>')
    setTimeout(function () {
      StartSearch();
    }, 200);
  }
}

function MakeUserMove() {
  if (UserMove.from !== SQUARES.NO_SQ && UserMove.to != SQUARES.NO_SQ) {
    console.log("User Move:" + PrSq(UserMove.from) + PrSq(UserMove.to));

    var parsed = ParseMove(UserMove.from, UserMove.to);

    DeselectSq(UserMove.from);
    DeselectSq(UserMove.to);

    console.log("Parsed:" + parsed);

    if (parsed !== NOMOVE) {
      MakeMove(parsed);
      MoveGUIPiece(parsed);
      CheckAndSet();
      PreSearch();
    }

    UserMove.from = SQUARES.NO_SQ;
    UserMove.to = SQUARES.NO_SQ;
  }
}

$(document).on('click', '.Piece', function (e) {
  console.log("Piece Click");
  if (srch_thinking === BOOL.FALSE && GameController.PlayerSide === brd_side) {
    if (UserMove.from === SQUARES.NO_SQ)
      UserMove.from = ClickedSquare(e.pageX, e.pageY);
    else
      UserMove.to = ClickedSquare(e.pageX, e.pageY);

    MakeUserMove();
  }
});

$(document).on('click', '.Square', function (e) {
  console.log("Square Click");
  if (srch_thinking === BOOL.FALSE && GameController.PlayerSide === brd_side && UserMove.from !== SQUARES.NO_SQ) {
    UserMove.to = ClickedSquare(e.pageX, e.pageY);
    MakeUserMove();
  }
});

function RemoveGUIPiece(sq) {
  //console.log("remove on:" + PrSq(sq));
  $(".Piece").each(function (index) {
    //console.log( "Picture:" + index + ": " + $(this).position().top + "," + $(this).position().left );
    if ((RanksBrd[sq] === 7 - Math.round($(this).position().top / 60)) && (FilesBrd[sq] === Math.round($(this).position().left / 60))) {
      //console.log( "Picture:" + index + ": " + $(this).position().top + "," + $(this).position().left );
      $(this).remove();
    }
  });
}

function AddGUIPiece(sq, pce) {
  const rank = RanksBrd[sq];
  const file = FilesBrd[sq];
  const rankName = "rank" + (rank + 1);
  const fileName = "file" + (file + 1);
  pieceFileName = "images/" + SideChar[PieceCol[pce]] + PceChar[pce].toUpperCase() + ".png";
  imageString = "<image src=\"" + pieceFileName + "\" class=\"Piece clickElement " + rankName + " " + fileName + "\"/>";
  //console.log("add on " + imageString);
  $("#Board").append(imageString);
}

function MoveGUIPiece(move) {
  const from = FROMSQ(move);
  const to = TOSQ(move);

  let flippedFrom = from;
  let flippedTo = to;
  let epWhite = -10;
  let epBlack = 10;

  if (GameController.BoardFlipped === BOOL.TRUE) {
    flippedFrom = MIRROR120(from);
    flippedTo = MIRROR120(to);
    epWhite = 10;
    epBlack = -10;
  }

  if (move & MFLAGEP) {
    let epRemove;
    if (brd_side === COLOURS.BLACK) {
      epRemove = flippedTo + epWhite;
    } else {
      epRemove = flippedTo + epBlack;
    }
    console.log("en pas removing from " + PrSq(epRemove));
    RemoveGUIPiece(epRemove);
  } else if (CAPTURED(move)) {
    RemoveGUIPiece(flippedTo);
  }

  const rank = RanksBrd[flippedTo];
  const file = FilesBrd[flippedTo];
  const rankName = "rank" + (rank + 1);
  const fileName = "file" + (file + 1);

  /*if(GameController.BoardFlipped == BOOL.TRUE) {
    rankName += "flip";
    fileName += "flip";
  }*/

  $(".Piece").each(function (index) {
    //console.log( "Picture:" + index + ": " + $(this).position().top + "," + $(this).position().left );
    if ((RanksBrd[flippedFrom] === 7 - Math.round($(this).position().top / 60)) && (FilesBrd[flippedFrom] === Math.round($(this).position().left / 60))) {
      //console.log("Setting pic ff:" + FilesBrd[from] + " rf:" + RanksBrd[from] + " tf:" + FilesBrd[to] + " rt:" + RanksBrd[to]);
      $(this).removeClass();
      $(this).addClass("Piece clickElement " + rankName + " " + fileName);
    }
  });

  if (move & MFLAGCA) {
    if (GameController.BoardFlipped === BOOL.TRUE) {
      switch (to) {
        case SQUARES.G1:
          RemoveGUIPiece(MIRROR120(SQUARES.H1));
          AddGUIPiece(MIRROR120(SQUARES.F1), PIECES.wR);
          break;
        case SQUARES.C1:
          RemoveGUIPiece(MIRROR120(SQUARES.A1));
          AddGUIPiece(MIRROR120(SQUARES.D1), PIECES.wR);
          break;
        case SQUARES.G8:
          RemoveGUIPiece(MIRROR120(SQUARES.H8));
          AddGUIPiece(MIRROR120(SQUARES.F8), PIECES.bR);
          break;
        case SQUARES.C8:
          RemoveGUIPiece(MIRROR120(SQUARES.A8));
          AddGUIPiece(MIRROR120(SQUARES.D8), PIECES.bR);
          break;
      }
    } else {
      switch (to) {
        case SQUARES.G1:
          RemoveGUIPiece(SQUARES.H1);
          AddGUIPiece(SQUARES.F1, PIECES.wR);
          break;
        case SQUARES.C1:
          RemoveGUIPiece(SQUARES.A1);
          AddGUIPiece(SQUARES.D1, PIECES.wR);
          break;
        case SQUARES.G8:
          RemoveGUIPiece(SQUARES.H8);
          AddGUIPiece(SQUARES.F8, PIECES.bR);
          break;
        case SQUARES.C8:
          RemoveGUIPiece(SQUARES.A8);
          AddGUIPiece(SQUARES.D8, PIECES.bR);
          break;
      }
    }
  }
  const prom = PROMOTED(move);
  console.log("PromPce:" + prom);
  if (prom !== PIECES.EMPTY) {
    console.log("prom removing from " + PrSq(flippedTo));
    RemoveGUIPiece(flippedTo);
    AddGUIPiece(flippedTo, prom);
  }

  printGameLine();
}

function DeselectSq(sq) {

  if (GameController.BoardFlipped === BOOL.TRUE) {
    sq = MIRROR120(sq);
  }

  $(".Square").each(function (index) {
    if ((RanksBrd[sq] === 7 - Math.round($(this).position().top / 60)) && (FilesBrd[sq] === Math.round($(this).position().left / 60))) {
      $(this).removeClass('SqSelected');
    }
  });
}

function SetSqSelected(sq) {

  if (GameController.BoardFlipped === BOOL.TRUE) {
    sq = MIRROR120(sq);
  }

  $(".Square").each(function (index) {
    //console.log("Looking Sq Selected RanksBrd[sq] " + RanksBrd[sq] + " FilesBrd[sq] " + FilesBrd[sq] + " position " + Math.round($(this).position().left/60) + "," + Math.round($(this).position().top/60));
    if ((RanksBrd[sq] === 7 - Math.round($(this).position().top / 60)) && (FilesBrd[sq] === Math.round($(this).position().left / 60))) {
      //console.log("Setting Selected Sq");
      $(this).addClass('SqSelected');
    }
  });
}

function StartSearch() {
  srch_depth = MAXDEPTH;
  const t = $.now();
  const tt = $('#ThinkTimeChoice').val();
  console.log("time:" + t + " TimeChoice:" + tt);
  srch_time = parseInt(tt) * 1000;
  SearchPosition();

  // TODO MakeMove here on internal board and GUI
  MakeMove(srch_best);
  MoveGUIPiece(srch_best);
  $('#ThinkingPng').remove();
  CheckAndSet();
}

$("#TakeButton").click(function () {
  console.log('TakeBack request... brd_hisPly:' + brd_hisPly);
  if (brd_hisPly > 0) {
    TakeMove();
    brd_ply = 0;
    SetInitialBoardPieces();
    $("#currentFenSpan").text(BoardToFen());
  }
});

$("#SearchButton").click(function () {
  GameController.PlayerSide = brd_side ^ 1;
  PreSearch();
});

$("#FlipButton").click(function () {
  GameController.BoardFlipped ^= 1;
  console.log("Flipped:" + GameController.BoardFlipped);
  SetInitialBoardPieces();
});

function NewGame() {
  ParseFen(START_FEN);
  PrintBoard();
  SetInitialBoardPieces();
  GameController.PlayerSide = brd_side;
  CheckAndSet();
  GameController.GameSaved = BOOL.FALSE;
}

$("#NewGameButton").click(function () {
  NewGame();
  newGameAjax();
});

function newGameAjax() {
  console.log('new Game Ajax');
  /*$.ajax({
    url : "insertNewGame.php",
    cache: false
    }).done(function( html ) {
      console.log('result:' + html);
    });*/
}

function initBoardSquares() {


  let light = 0;
  let rankName;
  let fileName;
  let divString;
  let lightString;
  let lastLight = 0;

  for (rankIter = RANKS.RANK_8; rankIter >= RANKS.RANK_1; rankIter--) {
    light = lastLight ^ 1;
    lastLight ^= 1;
    rankName = "rank" + (rankIter + 1);
    for (let fileIter = FILES.FILE_A; fileIter <= FILES.FILE_H; fileIter++) {
      fileName = "file" + (fileIter + 1);
      if (light === 0) lightString = "Light";
      else lightString = "Dark";
      divString = "<div class=\"Square clickElement " + rankName + " " + fileName + " " + lightString + "\"/>";
      //console.log(divString);
      light ^= 1;
      $("#Board").append(divString);
    }
  }
}

function ClearAllPieces() {
  console.log("Removing pieces");
  $(".Piece").remove();
}

function SetInitialBoardPieces() {
  let sq;
  let sq120;
  let file, rank;
  let rankName;
  let fileName;
  let imageString;
  let pieceFileName;
  let pce;
  ClearAllPieces();
  for (sq = 0; sq < 64; ++sq) {

    sq120 = SQ120(sq);

    pce = brd_pieces[sq120]; // crucial here

    if (GameController.BoardFlipped === BOOL.TRUE) {
      sq120 = MIRROR120(sq120);
    }

    file = FilesBrd[sq120];
    rank = RanksBrd[sq120];


    if (pce >= PIECES.wP && pce <= PIECES.bK) {
      rankName = "rank" + (rank + 1);
      fileName = "file" + (file + 1);

      pieceFileName = "images/" + SideChar[PieceCol[pce]] + PceChar[pce].toUpperCase() + ".png";
      imageString = "<image src=\"" + pieceFileName + "\" class=\"Piece " + rankName + " " + fileName + "\"/>";
      //console.log(imageString);
      $("#Board").append(imageString);
    }
  }
}